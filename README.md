# Custom Ghost Docker image build for Cité Politique

Used as a platform on which to edit content and manage users.

This custom build is important to hide elements we don't want to support in the admin editor
and to have a controlled upgrade between Ghost releases.
