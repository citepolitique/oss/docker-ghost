FROM ghost:2.23-alpine

COPY custom-admin.css current/core/built/assets

RUN sed -i "s|</head>|<link rel=\"stylesheet\" href=\"assets/custom-admin.css\"></head>|g" current/core/server/web/admin/views/default-prod.html \
    && sed -i "s|</head>|<link rel=\"stylesheet\" href=\"assets/custom-admin.css\"></head>|g" current/core/server/web/admin/views/default.html
